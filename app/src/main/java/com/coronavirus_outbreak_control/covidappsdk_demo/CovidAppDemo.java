package com.coronavirus_outbreak_control.covidappsdk_demo;

import org.coronavirus_outbreak_control.android.sdk.BuildConfig;
import org.coronavirus_outbreak_control.android.sdk.CovidApplicationSDK;


public class CovidAppDemo extends CovidApplicationSDK {

    private static final String TAG = "CovidApp";
    public void onCreate() {
        super.onCreate();
    }
    @Override
    public boolean isDebuging() {
        return BuildConfig.DEBUG;
    }
}
